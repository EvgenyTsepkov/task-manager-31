package ru.tsc.tsepkov.tm.api.repository;

import ru.tsc.tsepkov.tm.model.Project;

public interface IProjectRepository extends IUserOwnedRepository<Project> {

}
