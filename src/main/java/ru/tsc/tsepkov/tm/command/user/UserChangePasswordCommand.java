package ru.tsc.tsepkov.tm.command.user;

import org.jetbrains.annotations.NotNull;
import ru.tsc.tsepkov.tm.enumerated.Role;
import ru.tsc.tsepkov.tm.util.TerminalUtil;

public final class UserChangePasswordCommand extends AbstractUserCommand {

    @NotNull
    public static final String NAME = "change-user-password";

    @NotNull
    public static final String DESCRIPTION = "Change password of current user.";

    @NotNull
    @Override
    public String getName() {
        return NAME;
    }

    @NotNull
    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @Override
    public void execute() {
        final String userId = serviceLocator.getAuthService().getUserId();
        System.out.println("[USER CHANGE PASSWORD]");
        System.out.println("ENTER NEW PASSWORD:");
        @NotNull final String password = TerminalUtil.nextLine();
        serviceLocator.getUserService().setPassword(userId, password);
    }

    @NotNull
    @Override
    public Role[] getRoles() {
        return Role.values();
    }

}
