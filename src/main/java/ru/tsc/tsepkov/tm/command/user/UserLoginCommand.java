package ru.tsc.tsepkov.tm.command.user;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.tsc.tsepkov.tm.enumerated.Role;
import ru.tsc.tsepkov.tm.exception.user.AlreadyLoginException;
import ru.tsc.tsepkov.tm.util.TerminalUtil;

public final class UserLoginCommand extends AbstractUserCommand {

    @NotNull
    public static final String NAME = "login";

    @NotNull
    public static final String DESCRIPTION = "User login.";

    @NotNull
    @Override
    public String getName() {
        return NAME;
    }

    @NotNull
    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @Override
    public void execute() {
        if (serviceLocator.getAuthService().isAuth()) throw new AlreadyLoginException();
        System.out.println("[USER LOGIN]");
        System.out.println("ENTER LOGIN:");
        @NotNull final String login = TerminalUtil.nextLine();
        System.out.println("ENTER PASSWORD:");
        @NotNull final String password = TerminalUtil.nextLine();
        serviceLocator.getAuthService().login(login, password);
    }

    @Nullable
    @Override
    public Role[] getRoles() {
        return null;
    }

}
