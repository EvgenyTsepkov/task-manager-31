package ru.tsc.tsepkov.tm.enumerated;

import lombok.Getter;
import org.jetbrains.annotations.NotNull;

public enum Role {

    USUAL("Usual user"),
    ADMIN("Administrator");

    @Getter
    @NotNull
    private final String displayName;

    Role(@NotNull String displayName) {
        this.displayName = displayName;
    }

}
